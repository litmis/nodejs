// bash-4.3$ export PATH=/QOpenSys/QIBM/ProdData/OPS/Node6/bin:/QOpenSys/usr/bin                                                                           
// bash-4.3$ export LIBPATH=/QOpenSys/QIBM/ProdData/OPS/Node6/lib:/QOpenSys/usr/lib
// bash-4.3$ node --version
// v6.9.1
// bash-4.3$ node toolkit_async.js 
// [ { type: 'sh', data: '\nTue Aug 29 17:44:25 UTC 2017\nVine\n' } ]
// [ { type: 'sh', data: '\nTue Aug 29 17:44:27 UTC 2017\nJones\n' } ]
// connection pool golden rule 
// -- never share connection/statement across threads.
// -- async db2 uses threads (always)
global._xt = require('/QOpenSys/QIBM/ProdData/OPS/Node6/os400/xstoolkit/lib/itoolkit.js');
global._cb = function(result){ console.log(result); }
var conn1 = new global._xt.iConn("*LOCAL");
var conn2 = new global._xt.iConn("*LOCAL");
function task(conn,who,waitInterval) {
  conn.add(global._xt.iSh("sleep "+waitInterval+"; date; echo '"+who+"';"));
  conn.run(function (result) {
    var str_json = global._xt.xmlToJson(result);
    global._cb(str_json);
  });
  return 0;
};
var task1 = task(conn1,'Jones',5); // task 1 (async)
var task2 = task(conn2,'Vine',3); // task 2  (async)

