# Node.js Express Books Example

This project is an example Node.js application using express framework on IBM i.

## Purpose
- Demo a RESTful web application and connect to DB2 for i from Node.js using the [idb-pconnector](https://bitbucket.org/litmis/nodejs-idb-pconnector) library.

- Demo authentication on routes with the help of [passport.js](http://www.passportjs.org/)

- Demo appmetrics-dash

## Prerequisites

- Node.js
- g++
- python

Set Path
`export PATH=/QOpenSys/pkgs/bin:$PATH`

`yum install nodejs10 gcc-cplusplus python3`

setup `python` symlink to point to point to python3

`ln -s /QOpenSys/pkgs/bin/python3 /QOpenSys/pkgs/bin/python`

- gcc & python are needed to build appmetrics from source using `node-gyp`

## Getting Started

1) `git clone`

2) `npm install`

3) `npm setup`

4) `npm start`

## Populate appmetrics-dash

Simulate requests to the express books application with `demo.sh`

`usage: demo.sh host [PORT=4000]`

`demo.sh http://hostname.com`

# Issues

When running itoolkit on new system it is possible to get the following error:

`Character conversion between CCSID 1208 and CCSID 65535 not valid.`

To alleivate use `CHGUSRPRF USRPRF(USERNAME) CCSID(37)`


## Authors

* [Abdirahim Musse](https://github.com/abmusse)