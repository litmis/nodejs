# Tutorial 5: Putting it All Together

## Final Code Snippet

```javascript
//Express
const express = require('express');
const app = express();
const SCHEMA = 'BOOKSTORE';
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});
const port = process.env.PORT || 3001;


//View Engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use('/assets', express.static(__dirname+'/public'));

//DB2 Driver
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true});

//Authentication
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const ibmiAuth = require('./ibmiAuth').QSYSGETPH;

//Register middleware
app.use(session({
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: false,
  //use secure cookie if you have https enabled
  //cookie: {secure: true}
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next){
  console.log(`User: ${req.user} \n Authenticated: ${req.isAuthenticated()}`);
  res.locals.isAuthenticated = req.isAuthenticated();
  next();
});

//local strategy is called when passport.authenticate is called in post req for /login.
passport.use(new LocalStrategy(
  function(username, password, done) {
    ibmiAuth(username, password, (err, result) =>{
      if (result){
        console.log(username);
        return done(null, username);
      }
      return done(null, false);
    });
  }
));

passport.serializeUser(function(username, done) {
  done(null, username);
});

passport.deserializeUser(function(username, done) {
  done(null, username);
});

//setup the Database , call the function
const createBooksTable = require('./db');

//Routes

app.get('/', async (req, res) =>{
  res.redirect('/books');
});

//Respond with Login Page
app.get('/login', (req, res) => {
  res.render('login');
});

//Process Login
app.post('/login', urlencodedParser, passport.authenticate('local', {
  successRedirect: '/edit',
  failureRedirect: '/login'
}));

//Respond with Login Page
app.get('/logout', (req, res) => {
  req.logout();
  req.session.destroy();
  res.redirect('/');
});

//Respond with Static Book Table
app.get('/books', async (req, res) =>{
  try {
    let sql = `SELECT * FROM ${SCHEMA}.BOOKS`,
      title = 'ALL BOOKS',
      results;

    results = await pool.prepareExecute(sql);
    console.log(results);
    res.render('staticTable.ejs', {title: title, results: results} );

  }   catch (err){
    console.log(`Error SELECTING ALL BOOKS:\n${err.stack}`);
  }
});

// get a specific book by id
app.get('/getbook/:id', async (req, res) =>{
  try {
    let sql = `SELECT * FROM ${SCHEMA}.BOOKS WHERE bookid = ?`,
      results;

    results = await pool.prepareExecute(sql, [req.params.id]);

    if (results !== null){
      //return the book as json
      res.json(results);
    } else {
      res.send('no results');
    }
  }   catch (err){
    console.log(`Error SELECTING BY Book id:\n${err.stack}`);
  }

});

//Ensure User is Authenticated First
//Then Respond With Editable Table
app.get('/edit', checkLogin(), async (req, res) =>{
  try {
    let sql = `SELECT * FROM ${SCHEMA}.BOOKS`,
      title = 'ALL BOOKS',
      results;

    results = await pool.prepareExecute(sql);
    console.log(results);
    res.render('dynamicTable.ejs', {title: title, results: results} );
  }   catch (err){
    console.log(`Error SELECTING ALL BOOKS:\n${err.stack}`);
  }
});


//add a new book
app.post('/addbook', checkLogin(), urlencodedParser, async (req, res) =>{

  console.log(`${req.body.title} ${req.body.isbn} ${req.body.amount}`);
  //TODO validate form inputs
  let title = req.body.title,
    isbn = parseInt(req.body.isbn),
    amount = parseFloat(req.body.amount).toFixed(2);

    //add book to the DB
  try {
    let sql = `INSERT INTO ${SCHEMA}.BOOKS(title, isbn, amount) VALUES (?, ?, ?)`,
      results = null,
      book = {};

    results = await pool.prepareExecute(sql, [title, isbn, amount]);
    res.send(true);
  }   catch (err){
    res.send(false);
    console.log(`Error ADDING NEW BOOK:\n${err.stack}`);
  }

});

//update an exsisting book
app.put('/updateBook', checkLogin(), urlencodedParser, async (req, res) =>{
  try {
    let title = req.body.title,
      isbn = parseInt(req.body.isbn),
      amount = parseFloat(req.body.amount).toFixed(2),
      id = parseInt(req.body.id);

    let sql = `UPDATE ${SCHEMA}.BOOKS SET TITLE = ?, ISBN = ?, AMOUNT = ?
               WHERE BOOKID = ?`;

    await pool.prepareExecute(sql, [title, isbn, amount, id]);
    res.send(true);
  } catch (err){
    res.send(false);
    console.log(`Error UPDATING BOOK:\n${err.stack}`);
  }
});


//remove a book by its id
app.delete('/deletebook/:id', checkLogin(), async(req, res) =>{
  try {
    let sql = `DELETE FROM ${SCHEMA}.BOOKS WHERE BOOKID = ${req.params.id}`,
      connection = await pool.attach(),
      affectedRows;

    await connection.getStatement().prepare(sql);
    await connection.getStatement().execute();
    affectedRows = await connection.getStatement().numRows();

    if (affectedRows > 0){
      res.send(true);
    } else {
      res.send(false);
    }

  }   catch (err){
    console.log(`Error DELETING BOOK:\n${err.stack}`);
  }
});

function checkLogin(){
  return (req, res, next) =>{
    if (req.isAuthenticated()){
      console.log(`USER: ${req.session.passport.user}\n Session: ${JSON.stringify(req.session)}`);
      return next();
    }
    res.render('login', {error: 'Login First to Access Page'});
  };

}

app.listen(port, function(){
  console.log(`Bookstore listening @ ${address}:${port}`);
});

```