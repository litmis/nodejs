# Tutorial 4: Authentication

## Adding Authentication to End Points.

Before we can use any the Authentication packages we must require it as the following.

```javascript
//Express
const express = require('express');
const app = express();
const SCHEMA = 'BOOKSTORE';
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});
const port = process.env.PORT || 3001;


//View Engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use('/assets', express.static(__dirname+'/public'));

//DB2 Driver
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true});

//Authentication
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const ibmiAuth = require('./ibmiAuth').QSYSGETPH;
```

Now that we have a framework for our routes we need to provide authentication to endpoints we only want accessible to users who are logged in. To do so we will use sessions and cookies to maintain state between requests. There are many packages we can install to accomplish this goal but one popular choice is`express-sessions`.  [Express-sessions](https://www.npmjs.com/package/express-session) is middle ware which stores session data on the server and sets an encrypted cookie to the client. This will allow us to validate and check whether or not the user is authenticated. There is much to learn about sessions and cookies and I advise you to read the express-session [readme](https://github.com/expressjs/session). We will register and  configure our session to as follows:

**Warning** The default server-side session storage, `MemoryStore`, is _purposely_ not designed for a production environment. It will leak memory under most conditions, does not scale past a single process, and is meant for debugging and developing.

For a list of stores, see [compatible session stores](https://github.com/expressjs/session#compatible-session-stores).

**Warning**: This Project is meant to be run locally on your IBM i for development purposes. Use good judgement and do not test the project on an open port. By default HTTP is used therefore when submitting the login form the credentials could be sniffed. For Production Use HTTPS and set the cookie secure flag to true.


```javascript
app.use(session({
 
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: false,
  //use secure cookie if you have https enabled
  //cookie: {secure: true}
}));
```

 Secret is unique key to sign the cookie should be a random sequence of characters. It is recommended to store the secret within environment variable on your machine. That way you do not accidentally commit your secret to git hub , and your secret remains a secret. If `saveUninitialized` is set to true , Cookie will be given to user even if is not logged in , make sure this is set to false.

To validate are user we will check whether or not  the user is a user on our IBM i. We can do so with the help of [QSYGETPH API](https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/apis/QSYGETPH.htm). We can call the QSYGETPH API with the help of the [itoolkit](https://bitbucket.org/litmis/nodejs-itoolkit). Within the project files there is file called ibmiAuth.js this contains the logic to authenticate the user using the QSYGETPH API. Credit goes to Aaron Bartell for developing this function.

```javascript
function QSYSGETPH(user, pw, cb){
  let xt = require('itoolkit'),
    conn = new xt.iConn('*LOCAL'),
    pgm = new xt.iPgm('QSYGETPH', {lib:'QSYS', error:'off'});

  pgm.addParam(user.toUpperCase(), '10A');
  pgm.addParam(pw.toUpperCase(), '10A');
  pgm.addParam('', '12A', {io:'out', hex:'on'});
  let errno = [
    [0, '10i0'],
    [0, '10i0', {setlen:'rec2'}],
    ['', '7A'],
    ['', '1A']
  ];
  pgm.addParam(errno, {io:'both', len : 'rec2'});
  pgm.addParam(10, '10i0');
  pgm.addParam(0, '10i0');
  conn.add(pgm.toXML());
  conn.run(function(str) {
    let results = xt.xmlToJson(str);
    cb(null, results[0].success);
  }, true);
};

```

 Another package we will use to authenticate is `passport.js.`[Passport](http://www.passportjs.org/) will make authentication easier by providing built in functionalities to log the user in , log the user out , and check whether the user is authenticated. Passport has many different strategies to implement and even can authenticate with third parties such as Facebook , Twitter , and Google. For our purposes we will use a local strategy to authenticate a username and password. 

```javascript
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(
  function(username, password, done) {
    ibmiAuth(username, password, (err, result) =>{
      if (result){
        console.log(username);
        return done(null, username);
      }
      return done(null, false);
    });
  }
));

passport.serializeUser(function(username, done) {
  done(null, username);
});

passport.deserializeUser(function(username, done) {
  done(null, username);
});
```

`passport.initialize()` middleware is required to initialize Passport. passport.session\(\) middleware used to maintain persistent sessions. In order to support login sessions, Passport will serialize and deserialize 'user' instances to and from the session. In our case we only serialize the username making it accessible while the session is active. The most widely used way for websites to authenticate users is via a username and password. Support for this mechanism is provided by the passport-local module. Within Our local strategy we use the [QSYGETPH API](https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/apis/QSYGETPH.htm) to validate the user. If it result is true then the username is serialized within the session. Passport provides useful methods such as isAuthenticated that can be used to check wheter or not the user has has already logged in. We use this method to create a custom middleware called check login to verify login on protected endpoints.

```javascript
function checkLogin(){
  return (req, res, next) =>{
    if (req.isAuthenticated()){
      console.log(`USER: ${req.session.passport.user}\n Session: ${JSON.stringify(req.session)}`);
      return next();
    }
    res.render('login', {error: 'Must Login to Access Page'});
  };
}
```

And is called on the routes we want to protect. For Example:

```javascript
//Ensure User is Authenticated First
//Then Respond With Editable Table
app.get('/edit', checkLogin(), async (req, res) =>{
  try {
    let sql = `SELECT * FROM ${SCHEMA}.BOOKS`,
      title = 'ALL BOOKS',
      results;

    results = await pool.prepareExecute(sql);
    console.log(results);
    res.render('dynamicTable.ejs', {title: title, results: results} );
  }   catch (err){
    console.log(`Error SELECTING ALL BOOKS:\n${err.stack}`);
  }
});


//add a new book
app.post('/addbook', checkLogin(), urlencodedParser, async (req, res) =>{

  console.log(`${req.body.title} ${req.body.isbn} ${req.body.amount}`);
  //TODO validate form inputs
  let title = req.body.title,
    isbn = parseInt(req.body.isbn),
    amount = parseFloat(req.body.amount).toFixed(2);

    //add book to the DB
  try {
    let sql = `INSERT INTO ${SCHEMA}.BOOKS(title, isbn, amount) VALUES (?, ?, ?)`,
      results = null,
      book = {};

    results = await pool.prepareExecute(sql, [title, isbn, amount]);
    res.send(true);
  }   catch (err){
    res.send(false);
    console.log(`Error ADDING NEW BOOK:\n${err.stack}`);
  }

});

//update an exsisting book
app.put('/updateBook', checkLogin(), urlencodedParser, async (req, res) =>{
  try {
    let title = req.body.title,
      isbn = parseInt(req.body.isbn),
      amount = parseFloat(req.body.amount).toFixed(2),
      id = parseInt(req.body.id);

    let sql = `UPDATE ${SCHEMA}.BOOKS SET TITLE = ?, ISBN = ?, AMOUNT = ?
               WHERE BOOKID = ?`;

    await pool.prepareExecute(sql, [title, isbn, amount, id]);
    res.send(true);
  } catch (err){
    res.send(false);
    console.log(`Error UPDATING BOOK:\n${err.stack}`);
  }
});


//remove a book by its id
app.delete('/deletebook/:id', checkLogin(), async(req, res) =>{
  try {
    let sql = `DELETE FROM ${SCHEMA}.BOOKS WHERE BOOKID = ${req.params.id}`,
      connection = await pool.attach(),
      affectedRows;

    await connection.getStatement().prepare(sql);
    await connection.getStatement().execute();
    affectedRows = await connection.getStatement().numRows();

    if (affectedRows > 0){
      res.send(true);
    } else {
      res.send(false);
    }

  }   catch (err){
    console.log(`Error DELETING BOOK:\n${err.stack}`);
  }
});
```