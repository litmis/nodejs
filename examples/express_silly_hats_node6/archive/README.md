This is a quick node.js application calls IBM i via IBM node.js REST toolkit.

(last demo Doug Luebbe area meeting 2015-07-07)

### Links ###
* Web Application: http://myibminodejs.mybluemix.net/
* Git repository: https://hub.jazz.net/project/rangercairns/MyIBMiNodeJs/overview

You may download this code from the overview page (this page). 
Look for 'Branch: master', then click on the icon arrow just to right.

IBM i node.js itoolkit was simply copied from IBM i file system and added 
to this project (lib/*.js). Note: Using a very old version of IBM i itoolkit, 
you may want to update.


### IBM i XMLSERVICE (rest - xmlcgi.pgm) ###
The application uses a link to IBM i Yips machine supported by Common (thanks).
See lib/config.js file for Yips url port 80.
Note *NONE requires a special compile of xmlservice 
(for yips demos crtxml3.clp/plugconf3.rpgle).

* xmlservice doc: http://youngiprofessionals.com/wiki/index.php/XMLSERVICE/XMLSERVICE
* xmlservice git: https://bitbucket.org/inext/xmlservice-rpg

The Apache httpd.config:
```
#XML Toolkit http settings
ScriptAlias /cgi-bin/ /QSYS.LIB/XMLSERVICE.LIB/
<Directory /QSYS.LIB/XMLSERVICE.LIB/>
AllowOverride None
 order allow,deny
 allow from all
 SetHandler cgi-script
 Options +ExecCGI
</Directory>
#End XML Toolkit http settings

```
Note: A copy of **xmlservice** is compiled and supplied in **PTF** by DGO product 
(IBM i Apache). IBM i PTF library is QXMLSERV.LIB (latest DGO PTFs).
You would need to change XMLSERVICE.LIB to QXMLSERV.LIB in Apache httpd.conf (above).

### XMLSERVICE via DB2 (not demonstrated) ###
You may also use itoolkit remote to IBM i with DB2 Connect Enterprise Edition from Linux or Windows.
This product requires a purchased valid license to connect to IBM i (not free). 
Possibly talk with an IBM i sales representative for this option.
* db2 connect: http://www-03.ibm.com/software/products/en/db2connect-ee
* bluemix db2: https://console.ng.bluemix.net/catalog/services/ibm-db2-on-cloud


### Notes ###
```
===========
Quick git 3:
===========
git clone https://rangercairns:password@hub.jazz.net/git/rangercairns/MyIBMiNodeJs
$ cd MyIBMiNodeJs
(make changes to files.)
$ git add .
$ git commit -m 'some message changes'
$ git push

=========
Quick cf
=========
$ cf login -u adc@us.ibm.com -p ******
API endpoint> https://api.ng.bluemix.net
Authenticating...
OK
logs ...
cf logs MyIBMiNodeJs --recent
```

