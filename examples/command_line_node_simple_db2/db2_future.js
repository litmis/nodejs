// someday future via db2sock
// https://bitbucket.org/litmis/db2sock
global._db2 = require('/QOpenSys/QIBM/ProdData/OPS/Node6/os400/db2i/lib/db2a_future');
function task(who,waitInterval) {
  _db2.pool(function(conn) {
    conn.stmt(function(stmt) {
      sql = "select LSTNAM, STATE from QIWS.QCUSTCDT where LSTNAM='"+who+"'";
      stmt.prepare(sql, function() {
        stmt.execute(function(){
          stmt.fetchAll(function(result){ 
            setTimeout(function(){
              global._cb(result);
              delete stmt;
            }, waitInterval, result);
          });
        });
      });
    });
  });
  return 0;
};
var task1 = task('Jones',5000); // task 1 (async)
var task2 = task('Vine',3000); // task 2  (async)

