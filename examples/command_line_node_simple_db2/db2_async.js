// bash-4.3$ export PATH=/QOpenSys/QIBM/ProdData/OPS/Node6/bin:/QOpenSys/usr/bin                                                                           
// bash-4.3$ export LIBPATH=/QOpenSys/QIBM/ProdData/OPS/Node6/lib:/QOpenSys/usr/lib
// bash-4.3$ node --version
// v6.9.1
// bash-4.3$ node db2_async.js 
// [ { LSTNAM: 'Vine    ', STATE: 'VT' } ]
// [ { LSTNAM: 'Jones   ', STATE: 'NY' } ]
// connection pool golden rule 
// -- never share connection/statement across threads.
// -- async db2 uses threads (always)
global._db2 = require('/QOpenSys/QIBM/ProdData/OPS/Node6/os400/db2i/lib/db2a');
global._cb =function(result){ console.log(result); }
conn1 = new global._db2.dbconn();
conn1.conn("*LOCAL");
stmt1= new global._db2.dbstmt(conn1);
conn2 = new global._db2.dbconn();
conn2.conn("*LOCAL");
stmt2 = new global._db2.dbstmt(conn2);
function task(stmt,who,waitInterval) {
  sql = "select LSTNAM, STATE from QIWS.QCUSTCDT where LSTNAM='"+who+"'";
  stmt.prepare(sql, function(cb) {
    stmt.execute(function(){
      stmt.fetchAll(function(result){ 
        setTimeout(function(){
          global._cb(result);
          delete stmt;
        }, waitInterval, result);
      });
    });
  });
  return 0;
};
var task1 = task(stmt1,'Jones',5000); // task 1 (async)
var task2 = task(stmt2,'Vine',3000); // task 2  (async)

