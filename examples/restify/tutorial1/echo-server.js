var restify = require('restify');
var server = restify.createServer();

// echo the name back to the caller
server.get('/echo/:name', async function(req, res, next) {
  res.send("Hello, " + req.params.name);
  return next();
});

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});