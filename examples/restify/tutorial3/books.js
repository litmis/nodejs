let idb = require('./database').idb
let idbConnection = require('./database').connection;

module.exports = {

    getBook: async function(req, res, next) {

        let id = req.params.id;
        let dbStatement; 
        
        try {
            dbStatement = idbConnection.getStatement();
            var result = await dbStatement.exec("SELECT * FROM BOOKS WHERE ID = " + req.params.id +";");
            if (result.length > 0) {
            let responsePayload = { id: id, title: result[0]["TITLE"], author: result[0]["AUTHOR"] };
            res.send(responsePayload);
            } else {
            return next(new errors.NotFoundError("Could not find book with id " + id + " in the database."));
            }
        } catch (dbError) {
            console.error("Error is " + dbError);
            return next(new errors.InternalServerError("Internal Server Error"));
        } finally {
            dbStatement.close();
        }
        
        next();
    },

    postBook: async function(req, res, next) {

        // the body of the request should be in the format { title: 'title', author: 'author' }
        let payload = req.body;
        if (!(payload.hasOwnProperty('title') && payload.hasOwnProperty('author'))) {
            return next(new errors.BadRequestError("Your request wasn't formatted correctly. Pass a title and an author"));
        }

        let dbStatement; 
        try {
            dbStatement =  idbConnection.getStatement();

            let payload = req.body;

            // insert the book into the database
            await dbStatement.prepare("SELECT ID FROM FINAL TABLE(INSERT INTO BOOKS(TITLE, AUTHOR) VALUES(?, ?));");
            await dbStatement.bind([ [payload.title, idb.SQL_PARAM_INPUT, idb.SQL_BIND_CHAR], [payload.author, idb.SQL_PARAM_INPUT, idb.SQL_BIND_CHAR]]);
            await dbStatement.execute();
            let id = await dbStatement.fetch();
            await dbStatement.commit();

            // get the id from the new book, return it to the caller
            res.send( { id: id.ID });

        } catch (dbError) {
            console.error("Error is " + dbError);
            return next(new errors.InternalServerError("Internal Server Error"));
        } finally {
            dbStatement.close();
        }

        next();
    }
}