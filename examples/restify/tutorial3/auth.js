let idb = require('./database').idb
let idbConnection = require('./database').connection;
const jwt = require('jsonwebtoken');

module.exports = {

    authorize: async function(req, res, next) {

        let { username, password } = req.body;

        await authenticate(username, password).then(data => {

            let token = jwt.sign(data, process.env.SECRET_KEY, {
                expiresIn: '15m' // token expires in 15 minutes
            });

            res.send({ token });
            next();

        }).catch(function(error) {
            res.send(error);
            next();
        });
    }
}

// helper function, not exported
async function authenticate(username, password) {

    let dbStatement;

    dbStatement = await idbConnection.getStatement();
    await dbStatement.prepare("SELECT * FROM MARK.USERS WHERE USERNAME = ? AND PASSWORD = ?");
    await dbStatement.bind([ [username, idb.SQL_PARAM_INPUT, idb.SQL_BIND_CHAR], [password, idb.SQL_PARAM_INPUT, idb.SQL_BIND_CHAR]]);
    await dbStatement.execute();
    let user = await dbStatement.fetch();
    if (user) {
        return { username: user["USERNAME"] };
    } else {
        throw Error("Couldn't log you on. Check your credentials and try again");
    }
    dbStatement.close();
}
