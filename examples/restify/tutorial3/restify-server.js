require('dotenv').config();

const restify = require('restify');
const errors = require('restify-errors');

const restifyJwt = require('restify-jwt-community');
const jwt = require('jsonwebtoken');

const auth = require('./auth');
const books = require('./books');

// setting up the RESTify server
let server = restify.createServer();
server.use(restify.plugins.bodyParser());

server.use(restifyJwt({ "secret": process.env.SECRET_KEY }).unless({
  path: ['/auth/']
}));

// API ROUTES

// Auth
server.post('/auth/', auth.authorize);
// Books
server.get('/books/:id', books.getBook);
server.post('/books/', books.postBook);

// Start the server
server.listen(3030, function() {
  console.log('%s listening at %s', server.name, server.url);
});