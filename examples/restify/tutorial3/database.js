const idb = require('idb-pconnector');

const SCHEMA = 'MARK';
let idbConnection = new idb.Connection().connect();

setupDatabase();

async function setupDatabase() {

  // setting up the database tables
  try {
    var dbStatement = idbConnection.getStatement();
    await dbStatement.exec("SET schema " + SCHEMA);
    await dbStatement.exec("CREATE OR REPLACE TABLE BOOKS(ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), TITLE VARCHAR(32) NOT NULL, AUTHOR VARCHAR(32), PRIMARY KEY (ID))");
    await dbStatement.exec("CREATE OR REPLACE TABLE USERS(USERNAME VARCHAR(32) NOT NULL, PASSWORD VARCHAR(32) NOT NULL)");
    await dbStatement.exec("INSERT INTO USERS VALUES('Mark', 'pass123')");
  } catch (dbError) {
    console.error("Error is " + dbError);
  } finally {
    dbStatement.close();
  }
}

module.exports.idb = idb;
module.exports.connection = idbConnection;