var clients = require('restify-clients');

var client = clients.createJsonClient({
    url: 'http://localhost:3030',
    version: '*'
});

let token = null;

client.post('/auth/', { username: 'Mark', password: 'pass123' }, function(err, req, res, obj) {

    if (err) { console.error(err); return; }

    token = obj.token;

    let options = {
        path: '/books/',
        headers: {
            'Authorization': "Jwt " + token
        }
    }


    client.post(options, { title: "How to Develop Node.js", author: "Mark Irish" }, function(err, req, res, obj) {
        if (err) { console.error(err); return; }
    
        let options = {
            path: '/books/' + obj.id,
            headers: {
                'Authorization': "Jwt " + token
            }
        }

        client.get(options, function(err, req, res, obj) {
            if (err) { console.error(err); return; }

            console.log(obj);
        })
    
    });
});