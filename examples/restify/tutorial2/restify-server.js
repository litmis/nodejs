let restify = require('restify');
let errors = require('restify-errors');
let dba = require('idb-pconnector');

// setting up the RESTify server
let server = restify.createServer();
server.use(restify.plugins.bodyParser());

// DATABASE
// setting up our database variables
let dbaConnection = new dba.Connection().connect();
const SCHEMA = "MARK";

async function setupDatabaseTables() {
  // setting up the database tables
  try {
    var dbStatement = dbaConnection.getStatement();
    await dbStatement.exec("CREATE OR REPLACE TABLE " + SCHEMA + ".BOOKS(ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), TITLE VARCHAR(32) NOT NULL, AUTHOR VARCHAR(32), PRIMARY KEY (ID));");
  } catch (dbError) {
    console.error("Error is " + dbError);
  } finally {
    dbStatement.close();
  }
}

setupDatabaseTables();

// API ROUTES
// Books
server.get('/books/:id', async function(req, res, next) {

  let id = req.params.id;

  let dbStatement; 
  try {
    dbStatement = dbaConnection.getStatement();
    var result = await dbStatement.exec("SELECT * FROM " + SCHEMA + ".BOOKS WHERE ID = " + req.params.id +";");
    if (result.length > 0) {
      let responsePayload = { id: id, title: result[0]["TITLE"], author: result[0]["AUTHOR"] };
      res.send(responsePayload);
    } else {
      return next(new errors.NotFoundError("Could not find book with id " + id + " in the database."));
    }
  } catch (dbError) {
    console.error("Error is " + dbError);
    return next(new errors.InternalServerError("Internal Server Error"));
  } finally {
    dbStatement.close();
  }
  
  return next();
});

server.post('/books/', async function(req, res, next) {

  // the body of the request should be in the format { title: 'title', author: 'author' }
  let payload = req.body;
  if (!payload.hasOwnProperty('title') || !payload.hasOwnProperty('author')) {
    return next(new errors.BadRequestError("Your request wasn't formatted correctly. Pass a title and an author"));
  }

  let dbStatement; 
  try {
    dbStatement =  dbaConnection.getStatement();

    let payload = req.body;

    // insert the book into the database
    await dbStatement.prepare("SELECT ID FROM FINAL TABLE(INSERT INTO " + SCHEMA + ".BOOKS(TITLE, AUTHOR) VALUES(?, ?));");
    await dbStatement.bind([ [payload.title, dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR], [payload.author, dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR]]);
    await dbStatement.execute();
    let id = await dbStatement.fetch();
    await dbStatement.commit();

    // get the id from the new book, return it to the caller
    res.send(id.ID);

  } catch (dbError) {
      console.error("Error is " + dbError);
      return next(new errors.InternalServerError("Internal Server Error"));
  } finally {
      dbStatement.close();
  }

  next();
});

// Set up the server
server.listen(3030, function() {
  console.log('%s listening at %s', server.name, server.url);
});