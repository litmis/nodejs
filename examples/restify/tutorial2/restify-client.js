var clients = require('restify-clients');

var client = clients.createJsonClient({
    url: 'http://lp13ut28.rch.stglabs.ibm.com:3030/',
    version: '~1.0'
});

// post a new book, then get that book from the database to confirm it was inserted
client.post('/books/', { title: 'Foundation', author: "Isaac Asimov" }, function (err, req, res, obj) {

    console.log('Server returned: %j', obj);
    console.log("Use the returned ID to call our get route:");

    client.get('/books/' + obj, function (err, req, res, obj) {
        console.log('Server returned: %j', obj);
    });
});