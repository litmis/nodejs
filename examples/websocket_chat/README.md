
# Socket.IO Chat

A simple chat demo for socket.io

## How to use

```
$ cd websocket_chat
$ npm install
$ node .
```

# or override default port of 3000

```
$ cd websocket_chat
$ npm install
$ PORT=8001 node .
```

And point your browser to `http://localhost:3000`.

## Features

- Multiple users can join a chat room by each entering a unique username
on website load.
- Users can type chat messages to the chat room.
- A notification is sent to all users when a user joins or leaves
the chatroom.