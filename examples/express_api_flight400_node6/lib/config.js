//Need change based on your server configurations
var Database = "*LOCAL";
var User = "*NONE";
var Password = "*NONE";
var Host = "127.0.0.1";
var Port = 80;
var Path = "/cgi-bin/xmlcgi.pgm";
var IPC = "*na";
var CTL = "*here";
var DemoLib1 = "FLGHT400";
var DemoLib2 = "FLGHT400M";

exports.Database = Database;
exports.User = User;
exports.Password = Password;
exports.Host = Host;
exports.Port = Port;
exports.Path = Path;
exports.IPC = IPC;
exports.CTL = CTL;
exports.DemoLib1 = DemoLib1;
exports.DemoLib2 = DemoLib2;

