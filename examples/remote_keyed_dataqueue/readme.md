An example of how to access remote keyed data queues from node.js 
from a non-IBMi platform.  You need an Apache instance configured on
the IBMi using the included httpd.conf file.  You can configure your 
connection criteria in lib/config.js.

Run with "node app.js" 

