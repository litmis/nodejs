'use strict';

const Hapi = require('hapi');
const Path = require('path');
const Hoek = require('hoek');
const ibmi = require('./lib/ibmi');
const db = require('./models/db2ipool')
const pool = new db.Pool();

let uuid = 1;

const server = new Hapi.Server(
  { 
    connections: {
      routes: {
        files: {
          relativeTo: Path.join(__dirname, 'public')
        }
      }
    }
  }
);
server.connection({ 
  host: '0.0.0.0', 
  port: process.env.PORT || 8000
}); 

server.register(
  [require('vision'), require('hapi-auth-cookie'), require('inert')], 
  function(err){
  Hoek.assert(!err, err);

  server.views({ 
    engines: { pug: require('pug') },
    path: __dirname + '/views',
    compileOptions: {
      pretty: true
    }
  });  

  const cache = server.cache({ segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000 });
  server.app.cache = cache;
  server.auth.strategy('session', 'cookie', true, {
    password: 'password-should-be-32-charactersss-or-maybe-alot-more',
    cookie: 'hapi_ibmi_auth',
    redirectTo: '/login',
    redirectOnTry: false,
    isSecure: false,
    validateFunc: function (request, session, callback) {
      cache.get(session.sid, (err, cached) => {
        if (err) 
          return callback(err, false);                
        if (!cached) 
          return callback(null, false);
        request.isLoggedIn = true;
        return callback(null, true, cached.account);
      });
    }
  });

  server.route([
    {	method : 'GET', 
      path : '/{param*}', 
      config: { auth: false },
      handler : {			
        directory : {				
          path : '.',				
          listing : false,				
          index : true			
        }		
      }    
    },
    { method: 'GET',
      path:'/', 
      handler: function (request, reply) {   
        reply.view('index', {
          loggedIn: request.auth.isAuthenticated,
          title: 'Customer Search'
        });
      }
    },
    { method: 'GET',
      path:'/login',
      config: { 
        auth: { mode: 'try' }, 
        plugins: { 
          'hapi-auth-cookie': { redirectTo: false } 
        },
        handler: function (request, reply) {
          return reply.view('login', {  
            loggedIn: request.auth.isAuthenticated,          
            messages: 'To login please specify your profile and password.'
          });
        }
      }
    },
    {
      method: 'POST',
      path:'/login', 
      config: { auth: false },
      handler: function (request, reply) {  
        let profile = request.payload.profile
        let password = request.payload.password
        ibmi.QSYSGETPH(profile, password, function(err, result){
          if(result==true){
            const sid = String(++uuid);
            var account = {profile: profile, password: password};
            request.server.app.cache.set(sid, { account: account }, 0, (err) => {
              if (err) 
                return reply(err);
              request.cookieAuth.set({ sid: sid });
              return reply('Please wait while we log you in').redirect('customers');
            });                      
          } else {
            reply.view('login', {
              loggedIn: request.auth.isAuthenticated,
              profile: profile, 
              messages: 'Login failed.  Please try again.'
            }); 
          }
        })
      }
    },
    { 
      method: 'GET', 
      path: '/logout', 
      config: { 
        handler: function (request, reply) {
          request.cookieAuth.clear();
          return reply.redirect('/');
        }
      }
    },  
    {
      method: 'GET',
      path:'/customers', 
      handler: function (request, reply) {
        if(request.query.search_str==undefined){
          return reply.view('customers',{ 
            customers:[],
            loggedIn: request.auth.isAuthenticated,
          }); 
        }
        var search_str = request.query.search_str.toUpperCase();

        var sql = 
        `select * from QIWS.QCUSTCDT \
         WHERE (UPPER(CUSNUM) LIKE '%${search_str}%' or \ 
                UPPER(LSTNAM) LIKE '%${search_str}%' or \
                CAST(ZIPCOD AS VARCHAR(5)) LIKE '%${search_str}%') \
         LIMIT 20`;
        pool.easy(sql, function(result) {
          reply.view('customers', {
            loggedIn: request.auth.isAuthenticated,
            search_str: search_str, 
            customers: result,
            messages: ''
          });
        })
      }
    } 
  ]);
  server.start((err) => {
    if (err) 
      throw err;
    console.log('Server running at:', server.info.uri);
  });
});