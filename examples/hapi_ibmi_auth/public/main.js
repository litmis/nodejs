$('#search_str').keypress(function (e) {
  if (e.which == 13) {
    $('form#search').submit();
    return false;    
  }
});

$( "#csv_export" ).click(function() {
  var csvContent = "data:text/csv;charset=utf-8,";
  var json = JSON.parse($('#csv_export_data').html());
  csvContent += json.map(function(d){
    return JSON.stringify(d);
  })
  .join('\n') 
  .replace(/(^\{)|(\}$)/mg, '');
  window.open( encodeURI(csvContent) );  
});
