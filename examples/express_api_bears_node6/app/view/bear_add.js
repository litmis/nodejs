$(document).ready(function() {
  // bear add
  $(".bear_add").click(function() {        
    $.post("/zoo/api/bears", { name:$('.bear_name').val() }, function(query) {
      $('.bear_message').text(query.message);
      $('.bear_name').val("");
      location.reload(); 
    }) 
    .error(function(query) { alert(JSON.stringify(query.responseJSON.message)); }); // post
  }); // click
}); // ready

