# original information (obsolete move node 6)

# server_step_1.js - Initial server
First step in a good project, get anything to work. 
A good feeling when you see your browser connect to the project right off.

*** Setting Up Our Server server.js ***
```
$ cp server_step_1.js server.js
```

*** Starting Our Server and Testing ***
```
$ node server.js 
Magic happens on port 8080

http://myibmi:8080/api
{"message":"hooray! welcome to our api!"}
```


# bear_step_2.js - Initial REST model
Testing model before putting it all together is very useful. There are fine testing frameworks available, please use them.
However, for this simple tutorial a few custom tests fit my needs nicely.
```
$ cp app/models/bear_step_2.js app/models/bear.js
```

*** Testing ***
```
$ node test/test_bear_ctor.js
$ node test/test_bear_save.js
$ node test/test_bear_find.js
$ node test/test_bear_findById.js nbr
$ node test/test_bear_removeById.js nbr
```

# server_step_3.js - REST route and view
The tutorial tests with curl to act as a view component to check out REST JSON API. You could use any other 
REST capable language to consume JSON APIs bear project (php, ruby, python, etc.). Use of standard REST http
verbs like GET, POST, DELETE, etc., enables consistency across languages. When you move on to html/javascript
interface, you will find jQuery and other javascript elements exactly the same. All language API REST clients served with one
nodejs service (including browser), per customer requirement.
```
$ cp server_step_3.js server.js
$ node server.js 
Magic happens on port 8080
```

*** Testing ***
```
$ curl http://ut28p63:8080/api
{"message":"hooray! welcome to our api!"}

GET list
$ curl http://ut28p63:8080/api/bears
[{"ID":"2","NAME":"Brown"},{"ID":"3","NAME":"Brown"}

GET by id
$ curl http://ut28p63:8080/api/bears/3
[{"ID":"3","NAME":"Brown"}]

POST save name
$ curl -d "name=Sally" http://ut28p63:8080/api/bears

DELETE by id
$ curl -X DELETE http://ut28p63:8080/api/bears/3
```


# bear_step_4.js - Cache for performance
Bear cache is demonstrated in bear model to achieve customer requirements for 2000/hits a second (millions per hour). 
The trick is to understand relevance of time sensitive data 'truth' must favor consumer experience to build better web sites 
that use less machine resources (including database).
```
$ node server.js 
Magic happens on port 8080
```

*** before cache (ab tool test) ***
```
$ ab -t 15 -c 60 http://ut28p63:8080/api/bears/2
Concurrency Level:      60
Time taken for tests:   15.072 seconds
Complete requests:      1351
Failed requests:        0
Write errors:           0
Total transferred:      276606 bytes
HTML transferred:       37719 bytes
Requests per second:    89.64 [#/sec] (mean)
Time per request:       669.361 [ms] (mean)
Time per request:       11.156 [ms] (mean, across all concurrent requests)
Transfer rate:          17.92 [Kbytes/sec] received
```

*** copy model cache code ***
```
$ cp app/models/bear_step_4.js app/models/bear.js
```

*** after cache (ab tool test) ***
```
$ ab -t 15 -c 60 http://ut28p63:8080/api/bears/2                               
Concurrency Level:      60
Time taken for tests:   15.003 seconds
Complete requests:      38197
Failed requests:        0
Write errors:           0
Total transferred:      7563006 bytes
HTML transferred:       1031319 bytes
Requests per second:    2545.91 [#/sec] (mean)
Time per request:       23.567 [ms] (mean)
Time per request:       0.393 [ms] (mean, across all concurrent requests)
Transfer rate:          492.28 [Kbytes/sec] received
```

# view (admin -- outside chroot)
The Apache configuration below is a bit tricky. Look closely at /www/instance/htdocs symbolic links back 
into nodejs bear app/view project chroot location. Also take careful note of httpd.conf proxy/reverse proxy directory /api/.
This can be added to any existing Apache instance. Also sets a pattern for many nodejs chroot applications to be added to
any given Apache site.

*** config ***
```
> cp /QOpenSys/node4/home/node4/node4_tutorial_bears/conf/httpd.conf /www/apachedft/conf/.
> cd /www/apachedft/htdocs
> ln -sf /QOpenSys/node4/home/node4/node4_tutorial_bears/app/view bears
bash-4.3$ ls -l
lrwxrwxrwx    1 adc      0               112 Oct 20 10:06 bears -> /QOpenSys/node4/home/node4/node4_tutorial_bears/app/view
-rwx---r-x    1 qsys     0              1008 Oct  7 2009  index.html
```

*** run (browser) ***
```
http://ut28p63.rch.stglabs.ibm.com/bears/
```

# bear_step_optional_add_old_db2_driver.js - Optional try example with old db2 driver
A mapping class included in bear model should you want to use the old db2 driver.

*** optional step ***

```
$ export BEAR_DB_VERSION=1
$ node server.js          
moldy old db2 no-async
Magic happens on port 8080

$ unset BEAR_DB_VERSION 
$ node server.js       
new improved db2 async
Magic happens on port 8080
``` 



# chroot set-up (optional)
Project is designed to develop and deploy within a chroot with an existing Apache site. You may choose to develop in the root file system. 
However, i suggest you follow the extra chroot steps to truly understand the benefits of nodejs development project separation.
That is, when you use npm to augment your chroot nodejs project, really nice to know you are not clobbering some other nodejs application.

*** download latest ibmichroot ***
```
https://bitbucket.org/litmis/ibmichroot/downloads
$ ftp myibmi
ftp> bin
ftp> cd /home/(profile-admin)
ftp> put litmis-ibmichroot-xxxb.zip
ftp> quit
```

*** create node4 chroot profile (IBM i use profile-admin) ***
```
5250 (profile-admin)
> CRTUSRPRF USRPRF(NODE4) PASSWORD() USRCLS(*PGMR) TEXT('Tony Cairns')
> CHGUSRPRF USRPRF(NODE4) LOCALE(*NONE) HOMEDIR('/QOpenSys/node4/./home/node4')
```

*** create node4 chroot (IBM i use profile-admin) ***
```
$ ssh -X (profile-admin).myibmi
$ ksh
$ export PATH=.:/QOpenSys/QIBM/ProdData/OPS/tools/bin:/QOpenSys/usr/bin
$ bash
$ mkdir -p /QOpenSys/node4/home/node4
$ unzip litmis-ibmichroot-de6baa0c254b.zip 
$ mv litmis-ibmichroot-de6baa0c254b ibmichroot
$ cd ibmichroot/chroot
$ ./chroot_setup.sh chroot_minimal.lst /QOpenSys/node4 
$ ./chroot_setup.sh chroot_nls.lst /QOpenSys/node4 
$ ./chroot_setup.sh chroot_OPS_SC1.lst /QOpenSys/node
$ ./chroot_setup.sh chroot_gen_OPS_tools.lst /QOpenSys/node4
$ ./chroot_setup.sh chroot_gen_OPS_Node4.lst /QOpenSys/node4
```

*** make node4 owner of new chroot (IBM i use profile-admin) ***
```
$ chroot /QOpenSys/node4 /QOpenSys/usr/bin/ksh
$ export PATH=.:/QOpenSys/QIBM/ProdData/OPS/Node4/bin:/QOpenSys/QIBM/ProdData/OPS/tools/bin:/QOpenSys/usr/bin
$ bash
$ cd /
$ chown -Rh node4 /
$ exit
$ exit
$ pwd
/home/(profile-admin)/ibmichroot/chroot
```

# chroot build node4 app (node4 profile)
Our first task, npm load express and body-parser into our project. Specifically npm utility is downloading code from the internet.
This means your IBM i must be able to reach outside your company firewall to complete the task. Also, perhaps obvious,
you are trusting the code downloaded to behave appropriately in your web site (another reason i like chroot applications).

*** ssh to new chroot (node4 profile) ***
```
$ ssh -X node4@ut28p63
node4@ut28p63's password: 
$ ksh
$ export PATH=.:/QOpenSys/QIBM/ProdData/OPS/Node4/bin:/QOpenSys/QIBM/ProdData/OPS/tools/bin:/QOpenSys/usr/bin
$ bash
$ node --version
v4.4.6
$ 
```

