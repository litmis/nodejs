// server.js
// https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4
// (IBM i DB2 version of above link)
// =============================================================================

// BASE SETUP
// $ export UV_THREADPOOL_SIZE=64 
// =============================================================================
// add more worker threads uv async db operations
// process.env.UV_THREADPOOL_SIZE = 64;

// bear model
var bb = require('./app/models/bear');
var bear = new bb.Bear();      // create a new instance of the Bear model

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 47710;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
  // do logging
  // console.log('Something is happening.');
  next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working
router.get('/', function(req, res) {
  return res.json({ message: 'hooray! welcome to our api!' });   
});

// on routes that end in /bears
// ----------------------------------------------------
// GET:/zoo/api/bears
// http://myibmi/zoo/api/bears
router.route('/bears')
  // get all the bears
  .get(function(req, res) {
    bear.find(function(rsp) {
      res.statusCode = rsp["status"];
      return res.json(rsp);
    });
  });
// POST:/zoo/api/bears
// http://myibmi/zoo/api/bears
router.route('/bears')
  // create a bear
  .post(function(req, res) {
    // set the bears name (comes from the request req.body.name)
    // save the bear and check for errors
    bear.save(req.body.name, function(rsp) {
      res.statusCode = rsp["status"];
      return res.json(rsp);
    });
  });
// GET:/zoo/api/bears/bear_id
// http://myibmi/zoo/api/bears/4
router.route('/bears/:bear_id')
  // get the bear with that id
  .get(function(req, res) {
    // get the bear with id (comes from the request req.params.bear_id)
    bear.findById(req.params.bear_id, function(rsp) {
      res.statusCode = rsp["status"];
      return res.json(rsp);
    });
  });

// DELETE:/zoo/api/bears/bear_id
// http://myibmi/zoo/api/bears/4
router.route('/bears/:bear_id')
  // get the bear with that id
  .delete(function(req, res) {
    // get the bear with id (comes from the request req.params.bear_id)
    bear.removeById(req.params.bear_id, function(rsp) {
      res.statusCode = rsp["status"];
      return res.json(rsp);
    });
  });

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/zoo/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
// console.log('Magic happens on port ' + port);

