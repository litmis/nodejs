# Installing Koa

## Prerequisites

 **Node.js** and **npm** are both required before you can install **koa** on your system. If you don't yet have your environment setup, please follow the instructions on "~~Setting up your Node.js on IBM i"~~ before continuing with this tutorial.

## Installation

The source code for this project can be cloned or downloaded [here](https://bitbucket.org/litmis/nodejs/src/d6644e5ee3b54780b2fc9ffc8e74a3a01da1e614/examples/koa/?at=master). Once obtained simply run `npm install` within the project root to install the required node modules. Or continue following along to setup your project manually.

### New Project

If you are starting for scratch or want to test out koa before including it in your active node.js, you need to create a new directory and then initialize a new package. Run the following commands to get started:

```bash
mkdir koa-tutorials
cd koa-tutorials
npm init
npm install koa --save
```

When running `npm init`, the terminal will walk you through the creation of a **package.json** file. This file allows you to recreate all of the dependencies in your module simply by running `npm install.` For more about the power of the package.json file, consult [https://docs.npmjs.com/getting-started/using-a-package.json](https://docs.npmjs.com/getting-started/using-a-package.json).



The `--save` option on `npm install` will record the downloaded package as a dependency in your package.json file. Without passing this option, you won't have any record that your node module requires this package.

### Existing Project

If you want to add hapi to an existing project, simply navigate to that project's root directory and then install hapi

```bash
cd <your project's directory>
npm install koa --save
```