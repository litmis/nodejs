# Tutorial 1: A Simple Koa Server

## Creating a Koa Server

In your favorite code editor, create a file called **tutorial1.js** and write the following program \(adapted from [https://koajs.com/\#application](https://koajs.com/#application)\):

```javascript
const Koa = require('koa');
const app = new Koa();

app.use(async ctx => {
  ctx.body = 'Hello, Watson!';
});

app.listen(3000);
```

The above code creates a simple Koa server that will return "Hello, Watson!" when a user navigates to the base URL for your server. 

To run the above code, simply enter `node tutorial1.js` into the terminal from the root directory for your Koa project. Your server will then be running, and when you enter &lt;YourHostName&gt;:3000 in your browser, you should see "Hello, world" printed.

If another program is already using port 3000, you may see an error such as:

Error: listen EADDRINUSE :::3000

If so, you can change the port number in your index.js file. Just be use that in your browser you are navigating to the new address.

### The Koa Middleware Stack

When requests and responses flow through the Koa server, they are passed through a stack of middleware functions defined by the user. Below is a modified version of our simple server the utilizes this middleware stack:

```javascript
const Koa = require('koa');
const app = new Koa();

// logger
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

// response
app.use(async ctx => {
  ctx.body = 'Hello, Watson!';
});

app.listen(3000);
```

Like our first example, this code creates a simple server that displays "Hello, Watson!" However, the app also logs all requests through a simple logger function that notes how long the request took to process. It does this by storing a start time, then calling down the middleware stack by invoking the `next()` function on line 7. This suspends the logger function and passes control to the next middleware function defined, in this case the response. Because the response doesn't call another middleware, our simple stack is popped and control is returned upstream, in this case to the logger.

Run your Koa server in the terminal the same way that you ran the first example, using `node tutorial1.js.` When navigating to your server through the browser, you should still see "Hello, Watson!" printed on the screen. However, if you look at your terminal, you should see output similar to the following:

```text
GET / - 3
GET /favicon.ico - 0
```

This output is generated from our logger after a response is returned from the server, and tells us the HTTP method used \(in this case GET\), the url from the response \(/ and /favicon.ico, the former being the actual page and the latter being the icon displayed on the browser tab or bookmark\), and the time it took for each result to be returned, in milliseconds.

### Further Reading

This tutorial created a simple Koa server and demonstrated the power of the Koa middleware stack. But this tutorial only scratched the surface of the power of Koa as a web framework. More information about the capabilities of Koa can be found at [www.koajs.com](https://koajs.com/) and [https://github.com/koajs/examples](https://github.com/koajs/examples), and some good reading on why Koa can be thought of as "Express 5.0" can be found at [https://medium.com/@l1ambda/why-you-should-use-koa-with-node-js-7c231a8174fa](https://medium.com/@l1ambda/why-you-should-use-koa-with-node-js-7c231a8174fa).

In the next tutorial, we will connect to the internal DB2 database on our IBM i and query it in order to create resources with dynamic data. Finally, in Tutorial 3 we will create a simple login system using the local IBM i credentials to secure our Koa server.