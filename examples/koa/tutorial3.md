# Tutorial 3: Putting It All Together

Now that we know how to connect to our DB2 database using **Koa**, we need to be able to protect our database with some authentication. For this tutorial, we will be using [**passport**](http://www.passportjs.org/) ****along with your credentials on your IBM i system to require users to login before they can see certain pages. Our goal is to make a simple library inventory system where only users who are logged in and have a valid session can add books to the system \(we won't worry about editing or deleting books for now\).

We need a few additional packages on our Node.js server in order to route URIs to the correct page, and to handle the authentication, both on Koa's end and for us to interface with the IBM i. Before running this tutorial, you will have to install:

`npm install itoolkit --save  
npm install koa-bodyparser --save  
npm install koa-passport --save  
npm install koa-route --save  
npm install koa-router --save  
npm install koa-session --save  
npm install passport --save  
npm install passport-local --save`

Additionally, you should already have `idb-pconnector` and `koa` installed.

It may seem like a lot of packages to download. But notice most of them are Koa related. Koa's design philosophy is to only include the essential functionality in the main package to keep the install size small. All other functionality is delegated to packages that must be downloaded from npm.

With all of the packages installed, it is time to take a look at the two files that will make up our program. The first is a simple little program designed by Aaron Bartell that allows us to get to your IBM i credentials so that we can use them for login. The second is the entire web application, complete with routes and HTML pages.

```javascript
//consider converting to promise based.
function QSYSGETPH(user, pw, cb){
    var xt = require('itoolkit');
    var conn = new xt.iConn('*LOCAL');
    var pgm = new xt.iPgm('QSYGETPH', {lib:'QSYS', error:'off'});
    pgm.addParam(user.toUpperCase(), '10A');
    pgm.addParam(pw.toUpperCase(), '10A');
    pgm.addParam('', '12A', {io:'out', hex:'on'});
    let errno = [
      [0, '10i0'],
      [0, '10i0', {setlen:'rec2'}],
      ['', '7A'],
      ['', '1A']
    ];
    pgm.addParam(errno, {io:'both', len : 'rec2'});
    pgm.addParam(10, '10i0');
    pgm.addParam(0, '10i0');
    conn.add(pgm.toXML());
    conn.run(function(str) {
      var results = xt.xmlToJson(str);
      cb(null, results[0].success);
    }, true); // <---- set to sync for now because bug in iToolkit.  Hapijs hangs if this isn't done.
  };
  
  exports.QSYSGETPH = QSYSGETPH;
```

ibmiAuth.js simply exposes a function called QSYSGETPH that attempts to log into the IBM i system that your program is running on using the credentials you pass to it. It will return results if the login was successful, or nothing if your credentials were incorrect. For our program, we will be using this file to check user input on our login form before allowing them to access some of the resources on our server.

Next we have the entire application, including all the routes to pages and their contents. It looks like a lot, but I will walk through the important parts, especially as it relates to the IBM i.


```javascript
const Koa = require('koa')
const app = new Koa()
let dba = require('idb-pconnector');

// sessions
const session = require('koa-session')
app.keys = ['secretsquirrel']
app.use(session({}, app))

// body parser
const bodyParser = require('koa-bodyparser')
app.use(bodyParser())

// AUTHENTICATION
const passport = require('koa-passport');
const LocalStrategy = require('passport-local').Strategy;
const ibmiAuth = require('./ibmiAuth').QSYSGETPH;

passport.serializeUser(function(user, done) {
    done(null, user); 
});

passport.deserializeUser(function(user, done) {
   done(null, user);
});

passport.use(new LocalStrategy(
  function(username, password, done) {
    ibmiAuth(username, password, (err, result) =>{
      if (result){
        return done(null, username);
      }
      return done(null, false);
    });
  }
));

app.use(passport.initialize());
app.use(passport.session());

// DATABASE
// setting up our database variables
let dbaConnection = new dba.Connection().connect();
const SCHEMA = "MARK";

async function setupDatabaseTables() {
  // setting up the database tables
  try {
    var dbStatement = dbaConnection.getStatement();
    await dbStatement.exec("SET SCHEMA " + SCHEMA);
    await dbStatement.exec("CREATE OR REPLACE TABLE KOA_BOOKS(TITLE VARCHAR(100) NOT NULL, AUTHOR VARCHAR(100) NOT NULL);");
  } catch (dbError) {
    console.error("Error is " + dbError);
  } finally {
    dbStatement.close();
  }
}

setupDatabaseTables();

// ROUTES
const route = require('koa-route')

app.use(route.get('/', function(ctx) {
  ctx.type = 'html'
  ctx.body = `
  <form action="/login" method="post">
  <div class="container">
    <label for="username"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="username" required>

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>

    <button type="submit">Login</button>
  </div>
</form>
  `
}))

app.use(route.get('/dashboard', async function(ctx) {
  if (ctx.isAuthenticated()) {
    ctx.type = 'html';
    ctx.body = `
      Welcome to your dashboard
      <form action="/books" method="post">
        <div class="container">
          <label for="title"><b>Title</b></label>
          <input type="text" placeholder="Book Title" name="title" required>

          <label for="author"><b>Author</b></label>
          <input type="text" placeholder="Author Name" name="author" required>

        <button type="submit">Add Book</button>
      </div>
    </form>`;
    ctx.body += await getBooksTable();
    ctx.body +=  `<br><br><a href="/logout">LOGOUT</a>`
  } else {
    ctx.redirect('/unauthorized');
  }
}));

app.use(route.get('/unauthorized', function(ctx) {
  ctx.type = 'html';
  ctx.body = `
    You are not authorized to access that page
    <a href="/">Go back home.</a>
  `
}));

// POST /login
app.use(route.post('/login',
  passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/'
  })
));

// POST /book
app.use(route.post('/books', async function(ctx) {

  let dbStatement = dbaConnection.getStatement();
  await dbStatement.prepare("INSERT INTO KOA_BOOKS(TITLE, AUTHOR) VALUES(?, ?)");
  await dbStatement.bind([ [ctx.request.body.title, dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR], [ctx.request.body.author, dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR]]);
  await dbStatement.execute();
  await dbStatement.commit();

  ctx.redirect('/dashboard');
}));

// GET /logout 
app.use(route.get('/logout', function(ctx) {
  ctx.logout()
  ctx.redirect('/')
}));

// Helper function to generate books table
async function getBooksTable() {
  var table = '<table><tr><th>Title</th><th>Author</th></tr>';

  let dbStatement = dbaConnection.getStatement();
  // get the books from the database
  await dbStatement.prepare("SELECT * FROM KOA_BOOKS;");
  await dbStatement.execute();
  let results = await dbStatement.fetchAll();

  results.forEach(function(result) {
    table += "<tr><td>" + result["TITLE"] + "</td><td>" + result["AUTHOR"] + "</td></tr>";
  })

  table += '</table>'

  console.log("TABLE IS " + table);
  return table;
}

// start server
const port = process.env.PORT || 3006
app.listen(port, () => console.log('Server listening on', port));
```

The first thing we do is set up all of the variables we will need, usually importing them from packages that we have installed from npm. From lines 1 to 12, we declare variables like app and dba that represent our application and our database, respectively. We also declare a session and make a secret key, and tell the app that we want to use a body parser so we can get body information passed in our Koa context \(this is how we pass variables through our requests\).

From line 15 until line 39, we set up our authentication. We use passport.js as our main authentication framework and with it we use a local strategy \(username/password login using credentials on our system\). Passport.js is a lightweight and flexible framework for authenticating users and creating sessions. In order to authenticate users, you need to use a 'strategy' of authentication. These include things like logging in with google or twitter credentials, or using a username/password stored in your own database. We set up our strategy on lines 27 to 36, where we simply check the given credentials against our IBM i credentials. If the credentials are valid, we return the username to use as a key to serialize/deserialize the user across our site \(lines 19 and 23\). All this serializing and deserializing involves is storing our username in a session, and then using that username to get data \(in this case, just the username\) when the user goes to a page.

Lines 43 to 59 are setting up our database. As always, make sure to change the value of the SCHEMA on line 44 to the DB2 schema you are using.

We are looking to create a simple book inventory system. We need a login page and a dashboard where users can add books. We also need a page to let unauthenticated or unauthorized users know that they cannot access the dashboard. Finally, we need routes to add book data \(a POST\), to actually do the login logic \(a POST\), and to logout \(a GET\). With our site laid out conceptually, we use Koa to set up 6 routes. These routes are URIs that can be accessed through your browser or the webpage. We have 3 HTML pages, 2 routes that are POSTs that redirect the user depending on success/failure, and one GET route that simply logs out the user. Take a look at these routes, starting at line 62. Often, all we do is define our HTML method \(route**.get\(**..\)\), then say we are going to be returning html, and then return some pre-formatted text.

For our root page \('/'\) and dashboard \('/dashboard'\), we create forms that call our routes when data is submitted. When logging in through the root page, we call the /login route. When adding a book through the dashboard, we call the POST /books route \(where our actual code to add books to the database resides\).

In our login route, we call passport.authenticate, which takes our username and password combination passed in our form and either directs the user to the dashboard on success \(line 115\), or back to the homepage on failure \(line 116\). Then, we protect our routes by calling ctx.isAuthenticated\(line 82\) to make sure the user has access to a given page.

And that is pretty much it! In your terminal, simply run node tutorial3.js and you should see "Server listening on 3006". As long as your change your SCHEMA in the code and know the URL to your IBM i system, you should be able to go to &lt;your\_i\_URL&gt;:3006/ and see the login form. Log in with your IBM i credentials, and gain access to your site. If you try to go to /dashboard before logging in, you will see our unauthorized page, which will tell you that you need to log in and give you a link back to the homepage.

This tutorial was designed simply to show how you can use your IBM i credentials to login to a service, have that service save your session to keep you logged in across pages, and be able to query your DB2 database. In a real application, you would probably want to separate out your HTML files and use a template engine \(and also make your pages prettier with some CSS\), but I hope I have demonstrated the power of Node.js and Koa for creating applications, either for the internet or an internal intranet.

If there are things you still don't understand about Koa and passport, I recommend searching for documentation on those packages. Both frameworks are designed to be flexible, so there is a lot of rope to hang yourself with. But once you get the basics down, you will see that they can be used in almost any situation.