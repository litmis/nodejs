const Koa = require('koa')
const app = new Koa()
let dba = require('idb-pconnector');

// sessions
const session = require('koa-session')
app.keys = ['secretsquirrel']
app.use(session({}, app))

// body parser
const bodyParser = require('koa-bodyparser')
app.use(bodyParser())

// AUTHENTICATION
const passport = require('koa-passport');
const LocalStrategy = require('passport-local').Strategy;
const ibmiAuth = require('./ibmiAuth').QSYSGETPH;

passport.serializeUser(function(user, done) {
    done(null, user); 
});

passport.deserializeUser(function(user, done) {
   done(null, user);
});

passport.use(new LocalStrategy(
  function(username, password, done) {
    ibmiAuth(username, password, (err, result) =>{
      if (result){
        return done(null, username);
      }
      return done(null, false);
    });
  }
));

app.use(passport.initialize());
app.use(passport.session());

// DATABASE
// setting up our database variables
let dbaConnection = new dba.Connection().connect();
const SCHEMA = "MARK";

async function setupDatabaseTables() {
  // setting up the database tables
  try {
    var dbStatement = dbaConnection.getStatement();
    await dbStatement.exec("SET SCHEMA " + SCHEMA);
    await dbStatement.exec("CREATE OR REPLACE TABLE KOA_BOOKS(TITLE VARCHAR(100) NOT NULL, AUTHOR VARCHAR(100) NOT NULL);");
  } catch (dbError) {
    console.error("Error is " + dbError);
  } finally {
    dbStatement.close();
  }
}

setupDatabaseTables();

// ROUTES
const route = require('koa-route')

app.use(route.get('/', function(ctx) {
  ctx.type = 'html'
  ctx.body = `
  <form action="/login" method="post">
  <div class="container">
    <label for="username"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="username" required>

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>

    <button type="submit">Login</button>
  </div>
</form>
  `
}))

app.use(route.get('/dashboard', async function(ctx) {
  if (ctx.isAuthenticated()) {
    ctx.type = 'html';
    ctx.body = `
      Welcome to your dashboard
      <form action="/books" method="post">
        <div class="container">
          <label for="title"><b>Title</b></label>
          <input type="text" placeholder="Book Title" name="title" required>

          <label for="author"><b>Author</b></label>
          <input type="text" placeholder="Author Name" name="author" required>

        <button type="submit">Add Book</button>
      </div>
    </form>`;
    ctx.body += await getBooksTable();
    ctx.body +=  `<br><br><a href="/logout">LOGOUT</a>`
  } else {
    ctx.redirect('/unauthorized');
  }
}));

app.use(route.get('/unauthorized', function(ctx) {
  ctx.type = 'html';
  ctx.body = `
    You are not authorized to access that page
    <a href="/">Go back home.</a>
  `
}));

// POST /login
app.use(route.post('/login',
  passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/'
  })
));

// POST /book
app.use(route.post('/books', async function(ctx) {

  let dbStatement = dbaConnection.getStatement();
  await dbStatement.prepare("INSERT INTO KOA_BOOKS(TITLE, AUTHOR) VALUES(?, ?)");
  await dbStatement.bind([ [ctx.request.body.title, dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR], [ctx.request.body.author, dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR]]);
  await dbStatement.execute();
  await dbStatement.commit();

  ctx.redirect('/dashboard');
}));

// GET /logout 
app.use(route.get('/logout', function(ctx) {
  ctx.logout()
  ctx.redirect('/')
}));

// Helper function to generate books table
async function getBooksTable() {
  var table = '<table><tr><th>Title</th><th>Author</th></tr>';

  let dbStatement = dbaConnection.getStatement();
  // get the books from the database
  await dbStatement.prepare("SELECT * FROM KOA_BOOKS;");
  await dbStatement.execute();
  let results = await dbStatement.fetchAll();

  results.forEach(function(result) {
    table += "<tr><td>" + result["TITLE"] + "</td><td>" + result["AUTHOR"] + "</td></tr>";
  })

  table += '</table>'

  console.log("TABLE IS " + table);
  return table;
}

// start server
const port = process.env.PORT || 3006
app.listen(port, () => console.log('Server listening on', port));