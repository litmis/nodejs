# Summary

This series of tutorials created a simple Koa server and demonstrated the power of the Koa middleware stack. We also connected to a DB2 database to insert and retrieve data from our machine. Finally, we used passport.js, an open source authentication package, to enable access to resources on our Koa server only after users had logged in. 

These exercises demonstrate the power of Koa and other npm packages on the IBM i. But they have only scratched the surface of all Koa has to offer. If you are interested in learning about Koa, more information can be found at a number of websites online. And the best part is, all of them are possible on the IBM i!  Here are some helpful links to get you started:

[Koa - next generation web framework for node.js](https://koajs.com/)

[Koa.js Tutorial](https://www.tutorialspoint.com/koajs/index.htm)

[koajs/examples](https://github.com/koajs/examples)