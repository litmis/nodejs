# Koa on IBM i

**Koa** is a web framework designed to be more expressive and compact than other popular options such as express. Among the helpful properties of Koa is that it routes requests through a stack of middleware functions that are similar to standard programming call stacks are relieve the issues inherent in standard Node.js's "callback hell". 

This guide will walk you through setting up koa on your **IBM i** and demonstrate how to the framework to interact with important features of your IBM Sytem i such as DB2. From there, we encourage readers to consult the official Koa website \([https://koajs.com/](https://koajs.com/)\) for further guidance and inspiration for their development projects.