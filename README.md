![ibm i node.jpg](https://bitbucket.org/repo/GyyGoe/images/3795570392-ibm%20i%20node.jpg)

# Hey
## This is the IBM i Node.js water cooler.  

Check out the [/examples](https://bitbucket.org/litmis/nodejs/src/master/examples) directory for a bunch of prebuilt applications

Please select your flavor below (*click on the image*) to start the adventure on the wiki.


[![water_cooler_bacon.jpeg](https://bitbucket.org/repo/GyyGoe/images/2692012585-water_cooler_bacon.jpeg)](https://bitbucket.org/litmis/nodejs/wiki)